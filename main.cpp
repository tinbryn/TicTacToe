#include <iostream>
#include "Board.h"

int main()
{
    using namespace std;
    Board board;


    playPoint(board, {1, 1}, Token::cross);
    cout << board << '\n';
    playAsAI(board, Token::naught);
    cout << board << '\n';
    playPoint(board, {1, 0}, Token::cross);
    cout << board << '\n';
    playAsAI(board, Token::naught);
    cout << board << '\n';
    board.fill();
    cout << board << '\n';
    try
    {
        playAsAI(board, Token::naught);
    }
    catch (std::runtime_error &ex)
    {
        cout << "runtime error: " << ex.what();
    }

    return 0;
}

