//
// Created by kieran on 3/09/18.
//

#include "Board.h"
#include <algorithm>
#include <ostream>

Board::Board() noexcept : Array2D{}
{
    std::fill(begin(), end(), Token::empty);
}

bool Board::hasToken(int col, int row) const noexcept
{
    return elem(col, row) != Token::empty;
}

bool Board::isFull() const noexcept
{
    return std::all_of(begin(), end(), [](Token c) { return c != Token::empty; });
}

void Board::fill() noexcept
{
    std::replace(begin(), end(), Token::empty, Token::cross);
}

class TicTacToeException : public std::runtime_error
{
public:
    using std::runtime_error::runtime_error;
};

void playAsAI(Board &board, Token type)
{
    if (board.isFull())
    {
        throw TicTacToeException("Board is full, cannot play further");
    }
    for (int j = 0; j < 3; j++)
    {
        for (int i = 0; i < 3; i++)
        {
            Token &t = board.elem(i, j);
            if (t == Token::empty)
            {
                t = type;
                return;
            }
        }
    }

}

std::ostream &operator<<(std::ostream &stream, Token token)
{
    return stream << static_cast<char>(token);
}

std::ostream &operator<<(std::ostream &stream, const Board &board)
{
    auto printLine = [&](std::ostream &os, int n) -> std::ostream &
    {
        for (int j = 0; j < 2; j++)
        {
            os << board.elem(j, n) << '|';
        }
        os << board.elem(2, n) << '\n';
        return os;
    };
    for (int i = 0; i < 3; i++)
    {
        printLine(stream, i);
    }
    return stream;
}

void playPoint(Board &board, const Point &point, Token token)
{
    if (canPlayPosition(board, point.col, point.row))
    {
        board.elem(point.col, point.row) = token;
    }
}

bool canPlayPosition(const Board &board, int col, int row)
{
    return !board.hasToken(col, row);
}
