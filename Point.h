//
// Created by kieran on 3/09/18.
//

#ifndef TICTACTOE_POINT_H
#define TICTACTOE_POINT_H

struct Point
{
    int col;
    int row;
};

#endif //TICTACTOE_POINT_H
