//
// Created by kieran on 3/09/18.
//

#ifndef TICTACTOE_BOARD_H
#define TICTACTOE_BOARD_H

#include <array>
#include <iosfwd>
#include "Point.h"
#include "Array2D.h"

enum class Token : char
{
    empty = '-',
    cross = 'X',
    naught = 'O'
};

std::ostream &operator<<(std::ostream &stream, Token token);

class Board : protected Array2D<Token, 3, 3>
{
public:
    Board() noexcept;

    using Array2D::elem;

    bool hasToken(int col, int row) const noexcept;

    bool isFull() const noexcept;

    void fill() noexcept;
};

std::ostream &operator<<(std::ostream &stream, const Board &board);

//currently the AI is very very dumb
void playAsAI(Board &board, Token type);

void playPoint(Board &board, const Point &point, Token token);

bool canPlayPosition(const Board &board, int col, int row);


#endif //TICTACTOE_BOARD_H
