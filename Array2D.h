//
// Created by kieran on 4/09/18.
//

#ifndef TICTACTOE_ARRAY2D_H
#define TICTACTOE_ARRAY2D_H

#include <array>

template<typename T, int W, int H>
class Array2D : protected std::array<T, W * H>
{
    using base = std::array<T, W * H>;
public:
    static const int WIDTH = W;
    static const int HEIGHT = H;
    using base::begin;
    using base::cbegin;
    using base::rbegin;
    using base::crbegin;
    using base::end;
    using base::cend;
    using base::rend;
    using base::crend;

    T &elem(int col, int row) noexcept;

    const T &elem(int col, int row) const noexcept;
};

template<typename T, int N, int M>
T &Array2D<T, N, M>::elem(int col, int row) noexcept
{
    (*this)[col + WIDTH * row];
}

template<typename T, int N, int M>
const T &Array2D<T, N, M>::elem(int col, int row) const noexcept
{
    (*this)[col + WIDTH * row];
}


#endif //TICTACTOE_ARRAY2D_H
